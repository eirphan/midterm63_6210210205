import logo from './logo.svg';
import './App.css';

import HomePage from './HomePage';
import Program1 from './Program1';
import Program2 from './Program2';


function App() {
  return (
  
	<Router>
	
		<Switch>
			<Route exact path="/">
				<HomePage />
			</Route>
			
			<Route exact path="/">
				<Program1 />
			</Route>
			
			<Route exact path="/">
				<Program2 />
			</Route>
		</Switch>
	</Router>	
 	
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}
export default App;
export default App;
